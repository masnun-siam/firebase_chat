// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';

class ChatModel {
  final String message;
  final String id;
  final Timestamp time;
  final String email;

  ChatModel({
    required this.message,
    required this.email,
    required this.id,
    required this.time,
  });

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'message': message,
      'id': id,
      'time': time,
      'email': email,
    };
  }

  factory ChatModel.fromMap(Map<String, dynamic> map) {
    return ChatModel(
      message: map['message'] as String,
      id: map['id'] as String,
      time: map['time'],
      email: map['email'] as String,
    );
  }

  String toJson() => json.encode(toMap());

  factory ChatModel.fromJson(String source) => ChatModel.fromMap(json.decode(source) as Map<String, dynamic>);
}
