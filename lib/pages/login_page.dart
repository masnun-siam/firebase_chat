import 'package:flutter/material.dart';
import 'package:flutter_login/flutter_login.dart';

import '../services/authentication_service.dart';
import 'chat_page.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({super.key});

  @override
  Widget build(BuildContext context) {
    final authService = AuthService();
    return FlutterLogin(
      onLogin: (loginData) {
        return authService.login(
          email: loginData.name,
          password: loginData.password,
        );
      },
      onSignup: (signUpData) async {
        if (signUpData.name == null || signUpData.password == null) {
          return 'Please input email and password';
        }
        return authService.reginstration(
          email: signUpData.name!,
          password: signUpData.password!,
        );
      },
      onRecoverPassword: (email) {
        return authService.forgotPassword(email);
      },
      userType: LoginUserType.email,
      onSubmitAnimationCompleted: () {
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (_) => const ChatPage(),
          ),
        );
      },
    );
  }
}
