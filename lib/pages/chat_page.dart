import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_chat/models/chat_model.dart';
import 'package:firebase_chat/pages/login_page.dart';
import 'package:flutter/material.dart';

class ChatPage extends StatefulWidget {
  const ChatPage({super.key});

  @override
  State<ChatPage> createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  TextEditingController? controller;

  @override
  void initState() {
    super.initState();
    controller = TextEditingController();
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Chat Page'),
        actions: [
          IconButton(
            onPressed: () {
              FirebaseAuth.instance.signOut();
              Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(builder: (_) {
                  return const LoginPage();
                }),
                (route) => false,
              );
            },
            icon: Icon(Icons.logout),
          ),
        ],
      ),
      bottomNavigationBar: Row(
        children: [
          const SizedBox(width: 5),
          Expanded(
            child: TextField(
              controller: controller,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Message',
              ),
            ),
          ),
          const SizedBox(width: 5),
          IconButton(
              onPressed: () {
                if (controller!.text.isNotEmpty) {
                  final chatModel = ChatModel(
                    message: controller!.text,
                    email: FirebaseAuth.instance.currentUser!.email!,
                    id: FirebaseAuth.instance.currentUser!.uid,
                    time: Timestamp.now(),
                  );
                  FirebaseFirestore.instance.collection('chats').add(
                        chatModel.toMap(),
                      );
                }
              },
              icon: const Icon(Icons.send)),
          const SizedBox(width: 5),
        ],
      ),
      body: StreamBuilder<List<ChatModel>>(
        stream: FirebaseFirestore.instance
            .collection('chats')
            .orderBy('time', descending: true)
            .snapshots()
            .map(
              (querySnapshot) => querySnapshot.docs
                  .map(
                    (e) => ChatModel.fromMap(e.data()),
                  )
                  .toList(),
            ),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
          return ListView.separated(
            reverse: true,
            itemCount: snapshot.data!.length,
            separatorBuilder: (context, index) => const SizedBox(height: 10),
            itemBuilder: (context, index) {
              final isMe = snapshot.data![index].id ==
                  FirebaseAuth.instance.currentUser!.uid;
              return Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment:
                    isMe ? CrossAxisAlignment.end : CrossAxisAlignment.start,
                children: [
                  Text(snapshot.data![index].message),
                  Text(snapshot.data![index].email),
                  const SizedBox(width: double.infinity),
                ],
              );
            },
          );
        },
      ),
    );
  }
}

// List<int> value = [1, 2, 3, 4, 5];
// List<int> newValue = value.map((e) => e + 1).toList();
